#include <iostream>
namespace cool
{
  namespace PayloadMode{ enum Mode { INLINEPAYLOAD=0, SEPARATEPAYLOAD=1 }; }
  namespace PayloadMode2{ enum class Mode { INLINEPAYLOAD=2, SEPARATEPAYLOAD=3 }; }
  class StorageType{ public: enum TypeId { Bool=0, Int32=1, UInt32=2 }; };
  class StorageType2{ public: enum class TypeId { Bool=3, Int32=4, UInt32=5 }; };
  void dump()
  {
    std::cout << "cool::PayloadMode::SEPARATEPAYLOAD " << cool::PayloadMode::SEPARATEPAYLOAD << std::endl;
    std::cout << "cool::PayloadMode::Mode::SEPARATEPAYLOAD " << cool::PayloadMode::Mode::SEPARATEPAYLOAD << std::endl;
    std::cout << "(int)cool::PayloadMode2::Mode::SEPARATEPAYLOAD " << (int)cool::PayloadMode2::Mode::SEPARATEPAYLOAD << std::endl; // Need explicit cast to int
    std::cout << "(int)cool::PayloadMode2::SEPARATEPAYLOAD ";
    std::cout << "[NOT ALLOWED IN C++ OR ROOT]" << std::endl;
#ifdef FAILS
    std::cout << (int)cool::PayloadMode2::SEPARATEPAYLOAD << std::endl; // NOT ALLOWED IN C++ OR ROOT (COMPILATION FAILS)
#endif
    std::cout << std::endl;

    std::cout << "cool::StorageType::UInt32 " << cool::StorageType::UInt32 << std::endl;
    std::cout << "cool::StorageType::TypeId::UInt32 " << cool::StorageType::TypeId::UInt32 << std::endl;
    std::cout << "(int)cool::StorageType2::TypeId::UInt32 " << (int)cool::StorageType2::TypeId::UInt32 << std::endl; // Need explicit cast to int
    std::cout << "(int)cool::StorageType2::UInt32 ";
    std::cout << "[NOT ALLOWED IN C++ OR ROOT]" << std::endl;
#ifdef FAILS
    std::cout << (int)cool::StorageType2::UInt32 << std::endl; // NOT ALLOWED IN C++ OR ROOT (COMPILATION FAILS)
#endif
    std::cout << std::endl;
  }
}
