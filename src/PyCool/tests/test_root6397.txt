=== DUMP COMPILER VERSION
Using built-in specs.
COLLECT_GCC=/cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0native/x86_64-slc6/bin/g++
COLLECT_LTO_WRAPPER=/cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0native/x86_64-slc6/bin/../libexec/gcc/x86_64-pc-linux-gnu/6.2.0/lto-wrapper
Target: x86_64-pc-linux-gnu
Configured with: ../gcc-6.2.0/configure --prefix=/build/pmendez-sftnight/install-620local -with-system-zlib --disable-multilib --enable-languages=all
Thread model: posix
gcc version 6.2.0 (GCC) 

=== TEST COMPILATION WITH c++98
test_root6397.cpp:5:27: warning: scoped enums only available with -std=c++11 or -std=gnu++11
   namespace PayloadMode2{ enum class Mode { INLINEPAYLOAD=2, SEPARATEPAYLOAD=3 }; }
                           ^~~~
test_root6397.cpp:7:31: warning: scoped enums only available with -std=c++11 or -std=gnu++11
   class StorageType2{ public: enum class TypeId { Bool=3, Int32=4, UInt32=5 }; };
                               ^~~~
test_root6397.cpp: In function ‘void cool::dump()’:
test_root6397.cpp:11:84: error: ‘cool::PayloadMode::Mode’ is not a class or namespace
 < "cool::PayloadMode::Mode::SEPARATEPAYLOAD " << cool::PayloadMode::Mode::SEPARATEPAYLOAD << std::endl;
                                                                     ^~~~
test_root6397.cpp:12:96: error: ‘cool::PayloadMode2::Mode’ is not a class or namespace
 ::PayloadMode2::Mode::SEPARATEPAYLOAD " << (int)cool::PayloadMode2::Mode::SEPARATEPAYLOAD << std::endl; // Need explicit cast to int
                                                                     ^~~~
test_root6397.cpp:21:77: error: ‘cool::StorageType::TypeId’ is not a class or namespace
 :cout << "cool::StorageType::TypeId::UInt32 " << cool::StorageType::TypeId::UInt32 << std::endl;
                                                                     ^~~~~~
test_root6397.cpp:22:89: error: ‘cool::StorageType2::TypeId’ is not a class or namespace nt)cool::StorageType2::TypeId::UInt32 " << (int)cool::StorageType2::TypeId::UInt32 << std::endl; // Need explicit cast to int
                                                                     ^~~~~~

=== TEST COMPILATION WITH c++11

=== TEST COMPILATION WITH c++11 including the FAILS section
test_root6397.cpp: In function ‘void cool::dump()’:
test_root6397.cpp:16:23: error: ‘SEPARATEPAYLOAD’ is not a member of ‘cool::PayloadMode2’
     std::cout << (int)cool::PayloadMode2::SEPARATEPAYLOAD << std::endl; // NOT ALLOWED IN C++ OR ROOT (COMPILATION FAILS)
                       ^~~~
test_root6397.cpp:16:23: note: suggested alternative:
test_root6397.cpp:4:55: note:   ‘SEPARATEPAYLOAD’
   namespace PayloadMode{ enum Mode { INLINEPAYLOAD=0, SEPARATEPAYLOAD=1 }; }
                                                       ^~~~~~~~~~~~~~~
test_root6397.cpp:26:23: error: ‘UInt32’ is not a member of ‘cool::StorageType2’
     std::cout << (int)cool::StorageType2::UInt32 << std::endl; // NOT ALLOWED IN C++ OR ROOT (COMPILATION FAILS)
                       ^~~~

=== TEST COMPILATION IN ROOT

=== TEST EXECUTION IN ROOT
cool::PayloadMode::SEPARATEPAYLOAD 1
cool::PayloadMode::Mode::SEPARATEPAYLOAD 1
(int)cool::PayloadMode2::Mode::SEPARATEPAYLOAD 3
(int)cool::PayloadMode2::SEPARATEPAYLOAD [NOT ALLOWED IN C++ OR ROOT]

cool::StorageType::UInt32 2
cool::StorageType::TypeId::UInt32 2
(int)cool::StorageType2::TypeId::UInt32 5
(int)cool::StorageType2::UInt32 [NOT ALLOWED IN C++ OR ROOT]

cool.PayloadMode.SEPARATEPAYLOAD 1
cool.PayloadMode.Mode.SEPARATEPAYLOAD [NOT YET IN ROOT (ROOT-6397)]
cool.PayloadMode2.Mode.SEPARATEPAYLOAD [NOT YET IN ROOT (ROOT-6397)]
cool.PayloadMode2.SEPARATEPAYLOAD 3

cool.StorageType.UInt32 2
cool.StorageType.TypeId.UInt32 [NOT YET IN ROOT (ROOT-6397)]
cool.StorageType2.TypeId.UInt32 [NOT YET IN ROOT (ROOT-6397)]
cool.StorageType2.UInt32 5
