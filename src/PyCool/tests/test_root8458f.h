#ifndef COOLKERNEL_FIELDSELECTION4_H
#define COOLKERNEL_FIELDSELECTION4_H 1

#include <iostream> // Debug ROOT-8458
#include "CoolKernel/FieldSpecification.h"
#include "CoolKernel/IRecordSelection.h"
#include "CoolKernel/Record.h"

namespace cool
{
  class FieldSelection4 : virtual public IRecordSelection
  {
  public:
    enum Relation { EQ, NE, GT, GE, LT, LE };
    enum Nullness { IS_NULL, IS_NOT_NULL };
    virtual ~FieldSelection4(){}
    template<typename T> FieldSelection4( const std::string& name,
                                          const StorageType::TypeId typeId,
                                          Relation relation,
                                          const T& refValue );
    // IRecordSelection interface
    bool canSelect( const IRecordSpecification& ) const{ return true; }
    bool select( const IRecord& ) const{ return true; }
    IRecordSelection* clone() const{ return nullptr; }
  private:
    FieldSelection4();
    FieldSelection4( const FieldSelection4& rhs );
    FieldSelection4& operator=( const FieldSelection4& rhs );
  private:
    Record m_refValue;
    Relation m_relation;
  };

  //---------------------------------------------------------------------------

  template<typename T>
  inline FieldSelection4::FieldSelection4( const std::string& name,
                                           const StorageType::TypeId typeId,
                                           Relation relation,
                                           const T& refValue )
    : m_refValue( FieldSpecification( name, typeId ) )
    , m_relation( relation )
  {
#ifndef CRASH1
#pragma message("MAY NOT CRASH (add a printout)")
    std::cout << "FieldSelection for StorageType=" << typeId << "(" << StorageType::storageType(typeId).name() << "," << StorageType::storageType(typeId).cppType().name() << ") against " << name << "(" << typeid(T).name() << ")" << std::endl;
#else
#pragma message("WILL CRASH (do not add a printout)")
#endif
    m_refValue[0].setValue( refValue );
  }

  //---------------------------------------------------------------------------

  // Template instantiation
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Bool& b );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UChar& h );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Int16& s );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UInt16& t );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Int32& i );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UInt32& j );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Int64& x );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UInt64& y );
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Float& f );
  //=== WHEN a cout is included in FieldSelectil ctor, THEN:
  // - including this below causes this test NOT to throw and abort
  // - commenting out this line below causes this test to throw and abort
#ifndef CRASH2
#pragma message("MAY NOT CRASH (add a template)")
  template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Double& d );
#else
#pragma message("WILL CRASH (do not add a template)")
#endif
  //=== These three below are always irrelevant and I keep them out...
  //template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::String255& Ss );
  //template cool::FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Blob64k& N5coral4BlobE );

}
#endif // COOLKERNEL_FIELDSELECTION4_H
