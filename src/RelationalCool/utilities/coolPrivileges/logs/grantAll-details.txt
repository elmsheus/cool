* READER privileges

GRANT SELECT ON LCG_COOL.COOLTEST_DB_ATTRIBUTES TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_NODES TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_NODES_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_TAGS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_TAG2TAG TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_TAG2TAG_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_IOVS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0010_IOVS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0010_IOVS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0010_CHANNELS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0005_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0007_IOVS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0007_IOVS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0007_CHANNELS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0003_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0009_IOVS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0009_IOVS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0009_CHANNELS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0009_TAGS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0009_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0009_IOV2TAG TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0008_IOVS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0008_IOVS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0008_CHANNELS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0008_TAGS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0008_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0008_IOV2TAG TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0004_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0002_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0006_IOVS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0006_IOVS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0006_CHANNELS TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0001_TAGS_SEQ TO PUBLIC
GRANT SELECT ON LCG_COOL.COOLTEST_F0000_TAGS_SEQ TO PUBLIC

* WRITER privileges

GRANT INSERT ON LCG_COOL.COOLTEST_F0010_IOVS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0010_CHANNELS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0007_IOVS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0007_CHANNELS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0009_IOVS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0009_CHANNELS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0008_IOVS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0008_CHANNELS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0006_IOVS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0006_CHANNELS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_IOVS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0010_IOVS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0010_IOVS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0010_CHANNELS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0007_IOVS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0007_IOVS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0007_CHANNELS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0009_IOVS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0009_IOVS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0009_CHANNELS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0008_IOVS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0008_IOVS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0008_CHANNELS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0006_IOVS TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0006_IOVS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0006_CHANNELS TO PUBLIC

* TAGGER privileges

GRANT INSERT ON LCG_COOL.COOLTEST_TAGS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_TAG2TAG TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0009_TAGS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0009_IOV2TAG TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0008_TAGS TO PUBLIC
GRANT INSERT ON LCG_COOL.COOLTEST_F0008_IOV2TAG TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_TAG2TAG_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0005_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0003_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0009_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0008_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0004_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0002_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0001_TAGS_SEQ TO PUBLIC
GRANT UPDATE ON LCG_COOL.COOLTEST_F0000_TAGS_SEQ TO PUBLIC
GRANT DELETE ON LCG_COOL.COOLTEST_TAGS TO PUBLIC
GRANT DELETE ON LCG_COOL.COOLTEST_TAG2TAG TO PUBLIC
GRANT DELETE ON LCG_COOL.COOLTEST_F0009_TAGS TO PUBLIC
GRANT DELETE ON LCG_COOL.COOLTEST_F0009_IOV2TAG TO PUBLIC
GRANT DELETE ON LCG_COOL.COOLTEST_F0008_TAGS TO PUBLIC
GRANT DELETE ON LCG_COOL.COOLTEST_F0008_IOV2TAG TO PUBLIC
