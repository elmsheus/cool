
// Include files
#include <iostream>
#include "CoolKernel/Exception.h"
#include "CoolKernel/InternalErrorException.h"
#include "CoralBase/MessageStream.h"
#include "RelationalAccess/AuthenticationServiceException.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/IDatabaseServiceSet.h"
#include "RelationalAccess/IDatabaseServiceDescription.h"
#include "RelationalAccess/ILookupService.h"

// Local include files
#include "../../src/RalSessionMgr.h"
#include "../../src/RelationalDatabaseId.h"

// Namespace
using namespace cool;

// Message output
#define LOG std::cout
#define DEB if ( false ) std::cout << "DEBUG: " // DEBUG printout (disabled)

//-----------------------------------------------------------------------------

coral::ILookupService& lookupSvc()
{
  coral::ConnectionService connSvc;
  return connSvc.configuration().lookupService(); // CORALCOOL-2252
}

//-----------------------------------------------------------------------------

coral::IAuthenticationService& authSvc()
{
  coral::ConnectionService connSvc;
  return connSvc.configuration().authenticationService(); // CORALCOOL-2252
}

//-----------------------------------------------------------------------------

void authenticate( const std::string& coralReplica,
                   const std::string& technology,
                   std::string& user,
                   std::string& password )
{
  try
  {
    DEB << "Get credentials for '" << coralReplica << "'" << std::endl;
    // Get the authentication credentials from CORAL
    const coral::IAuthenticationCredentials& cred =
      authSvc().credentials( coralReplica );
    user = cred.valueForItem( "user" );
    password = cred.valueForItem( "password" );
    // Do not allow empty usernames for Kerberos non-proxy authentication
    if ( user == "" )
      throw Exception
        ( "Empty username credentials found for " + coralReplica,
          "coolAuthentication" );
    // Allow empty password for Kerberos proxy authentication (task #30593)
    /*
    if ( password == "" )
      throw Exception
        ( "Empty password credentials found for " + coralReplica,
          "coolAuthentication" );
    *///
    if ( user == "" && password != "" )
      throw Exception
        ( "Empty username credentials with non-empty password found for "
          + coralReplica, "coolAuthentication" );
  }
  catch( coral::UnknownConnectionException& /*exc*/ )
  {
    if ( technology == "sqlite" || technology == "frontier" )
    {
      //LOG << "WARNING: '" << exc.what() << "'" << std::endl;
      //LOG << "WARNING: set DUMMY user and DUMMY password" << std::endl;
      user = "DUMMY";
      password = "DUMMY";
    }
    else throw;
  }
}

//-----------------------------------------------------------------------------

void printOut( const std::string& coralReplica,
               const std::string& coralMode,
               const std::string& middleTier,
               const std::string& technology,
               const std::string& server,
               const std::string& schema,
               const std::string& user,
               const std::string& password,
               bool hidepw )
{
  // Print out the database server properties
  // [NB coralReplica includes the middleTier if present]
  // [NB technology, server, schema, user/pswd are after the MT if it exists]
  LOG << "==> coralReplica = " << coralReplica << std::endl;
  LOG << "==> coralMode    = " << coralMode << std::endl;
  LOG << "==> middleTier   = " << middleTier << std::endl;
  LOG << "==> technology   = " << technology << std::endl;
  LOG << "==> server       = " << server << std::endl;
  LOG << "==> schema       = " << schema << std::endl;

  // Print out the authentication credentials
  //LOG << "CORAL authentication credentials:" << std::endl;
  LOG << "==> user         = " << user << std::endl;
  if ( hidepw )
    LOG << "==> password     = " << "********" << std::endl;
  else
    LOG << "==> password     = " << password << std::endl;
}

//-----------------------------------------------------------------------------

int main( int argc, char** argv )
{

  // *** TRY ***

  try {

    // Get the COOL database identifier
    bool hidepw = false;
    int nhidepw = 0;
    bool rwOnly = false;
    bool firstOnly = false;
    std::string dbId;
    if ( argc > 1  && std::string(argv[1]) == "-hidepw" )
    {
      hidepw = true;
      nhidepw = 1;
    }
    if ( argc == 3+nhidepw && std::string(argv[1+nhidepw]) == "-1" )
    {
      firstOnly = true;
      dbId = argv[2+nhidepw];
    }
    else if ( argc == 3+nhidepw && std::string(argv[1+nhidepw]) == "-1RW" )
    {
      rwOnly = true;
      firstOnly = true;
      dbId = argv[2+nhidepw];
    }
    else if ( argc == 2+nhidepw )
    {
      dbId = argv[1+nhidepw];
    }
    else
    {
      LOG << "Usage: " << argv[0] << " [-hidepw] [-1|-1RW] dbId" << std:: endl;
      LOG << "[Option -1: display only the first replica]" << std::endl;
      LOG << "[Option -1RW: display only the first RW replica]" << std::endl;
      std::string dbId1 =
        "oracle://SERVER;schema=SCHEMA;user=USER;password=PSWD;dbname=DB";
      std::string dbId2 =
        "oracle://SERVER;schema=SCHEMA;dbname=DB";
      std::string dbId3 =
        "alias/DB";
      LOG << std::endl;
      LOG << "Example: " << argv[0] << " '" << dbId1 << "'\n"
          << "[Decode user and password from explicit credentials]"
          << std::endl;
      LOG << std::endl;
      LOG << "Example: " << argv[0] << " '" << dbId2 << "'\n"
          << "[Get user and password from authentication service]"
          << std::endl;
      LOG << std::endl;
      LOG << "Example: " << argv[0] << " '" << dbId3 << "'\n"
          << "[Get alias translation from dblookup service]"
          << std::endl
          << "[Get user and password from authentication service]"
          << std::endl;
      return 1;
    }

    // --- TRANSLATE COOL URL into CORAL alias or explicit replica

    // Parse the dbId URL as RelationalDatabaseId connection parameters
    RelationalDatabaseId relationalDbId( dbId );

    // Print out the database identification
    // [NB If MT is followed by an alias, throw (need remote dblookup.xml!)]
    // [NB If MT exists, get user/pswd with local auth.xml (CORALCOOL-2049)]
    LOG << "COOL databaseId" << std::endl;
    LOG << "==> middleTier   = "
        << relationalDbId.middleTier() << std::endl;
    LOG << "==> url          = "
        << relationalDbId.url() << std::endl;
    LOG << "==> urlHidePswd  = "
        << relationalDbId.urlHidePswd() << std::endl;

    // Extract the connection parameters from the RelationalDbId
    std::string middleTier ( relationalDbId.middleTier() );
    std::string technology ( relationalDbId.technology() );
    std::string server     ( relationalDbId.server()     );
    std::string schema     ( relationalDbId.schema()     );
    std::string dbName     ( relationalDbId.dbName()     );
    std::string user       ( relationalDbId.user()       );
    std::string password   ( relationalDbId.password()   );

    // PANIC if the database name is not given explicitly
    // (RelationalDatabaseId should have already failed!)
    if ( dbName == "" )
      throw InternalErrorException
        ( "PANIC! No database name specified in COOL database URL",
          "coolAuthentication" );
    LOG << "==> dbName       = "
        << dbName << std::endl;

    // Get the CORAL connection string
    std::string coralUrl;
    if ( middleTier == "" )
      coralUrl = RalConnectionString( relationalDbId );
    else
      coralUrl = RalConnectionString( relationalDbId.url() );
    std::string coralMode = "N/A";

    // --- Case 1: CORAL alias (loop over replicas)
    // Example: "COOL-CoralServer-Oracle-avalassi/CFC9B408"
    // Example (not supported): "coral://atlas-coral-01:40007&COOL-CoralServer-Oracle-avalassi/CFC9B408"

    // If connection string looks like an alias, pass it to the LookupService
    if ( coralUrl.find("://") == std::string::npos &&
         coralUrl.substr(0,12) != "sqlite_file:" )
    {
      LOG << "CORAL logical alias" << std::endl;
      LOG << "==> coralAlias   = " << coralUrl << std::endl;

      // PANIC if technology, server, schema, user or password
      // are defined at this point (this should never happen!)
      if ( technology != "" )
        throw InternalErrorException
          ( "PANIC! COOL URL based on CORAL alias explicitly contains "
            "technology '" + technology + "'",
            "coolAuthentication" );
      if ( server != "" )
        throw InternalErrorException
          ( "PANIC! COOL URL based on CORAL alias explicitly contains "
            "server '" + server + "'",
            "coolAuthentication" );
      if ( schema != "" )
        throw InternalErrorException
          ( "PANIC! COOL URL based on CORAL alias explicitly contains "
            "schema '" + schema + "'",
            "coolAuthentication" );
      if ( user != "" )
        throw InternalErrorException
          ( "PANIC! COOL URL based on CORAL alias explicitly contains "
            "user '" + user + "'",
            "coolAuthentication" );
      if ( password != "" )
        throw InternalErrorException
          ( "PANIC! COOL URL based on CORAL alias explicitly contains "
            "password '" + password + "'",
            "coolAuthentication" );

      // Drop support for alias after middleTier (2016 fix for CORALCOOL-2049)
      if ( middleTier != "" )
        throw Exception
          ( "COOL URL containing a CORAL alias after a middle tier '" +
            middleTier + "' is not supported",
            "coolAuthentication" );

      // Lookup the replicas
      coral::AccessMode mode = ( rwOnly ? coral::Update : coral::ReadOnly );
      std::auto_ptr<coral::IDatabaseServiceSet> replicas
        ( lookupSvc().lookup( coralUrl, mode ) );
      int nReplicas = replicas->numberOfReplicas();
      if ( nReplicas  == 0 )
        throw Exception
          ( "No dblookup replicas found for " + coralUrl,
            "coolAuthentication" );

      // Loop over the replicas
      // Reuse variables coralUrl, technology, server, schema, user, password
      DEB << "Loop over " << nReplicas
          << " replicas for alias '" << coralUrl << "'" << std::endl;
      for ( int iReplica = 0; iReplica < nReplicas; iReplica++ )
      {
        const coral::IDatabaseServiceDescription&
          dsd = replicas->replica( iReplica );
        coralUrl = dsd.connectionString();
        coralMode = ( dsd.accessMode() == coral::ReadOnly ? "R/O" : "R/W" );
        DEB << "Analyse replica '" << coralUrl << "'" << std::endl;

        // Does the dblookup replica point to a middle-tier?
        // [NB From now on the REMOTE coralUrl may differ from coralFullUrl]
        std::string coralFullUrl = coralUrl;
        middleTier = RelationalDatabaseId::stripMiddleTier( coralUrl );

        // Extract technology, server and schema
        // [NB IF MT exists, these are the REMOTE technology, server, schema]
        if ( coralUrl.substr(0,12) == "sqlite_file:" )
        {
          // Special case for SQLite
          technology = "sqlite";
          server = "";
          schema = coralUrl.substr(12);
        }
        else
        {
          // Backends other than SQLite
          std::string::size_type pos = coralUrl.find("://");
          if ( pos == std::string::npos )
            throw Exception
              ( "The connection string returned by the lookup service "
                "does not contain \"://\": " + coralUrl,
                "coolAuthentication" );
          technology = coralUrl.substr(0,pos);
          pos += 3;
          std::string::size_type pos2 = coralUrl.find_last_of('/');
          if ( pos2 == std::string::npos )
            throw Exception
              ( "The connection string returned by the lookup service "
                "does not contain the schema name: " + coralUrl,
                "coolAuthentication" );
          server = coralUrl.substr(pos,pos2 - pos);
          schema = coralUrl.substr(pos2+1);
        }

        // Retrieve user/password using the authentication service
        // [NB If MT exists, retrieve user/pswd with local auth.xml as these
        // are needed in sqlplus spawned by regression tests (CORALCOOL-2049)]
        user = "";
        password = "";
        authenticate( coralUrl, technology, user, password );
        
        // Print out the results
        LOG << "CORAL physical replica ("
            << iReplica+1 << " of " << nReplicas << ")" << std::endl;
        printOut( coralFullUrl, coralMode, middleTier,
                  technology, server, schema, user, password, hidepw );

        // Use only the first replica if required
        if ( firstOnly ) break;

      }

    }

    // --- Case 2: explicit CORAL replica
    // Example: "coral://coralsrv01:40007&oracle://lcg_cool_nightly;schema=lcg_cool;dbname=COOLTEST"

    else

    {

      // If the user name or password are not given explicitly,
      // retrieve them using the CORAL authentication service
      if ( user == "" || password == "" )
        authenticate( coralUrl, technology, user, password );

      // Print out the results
      LOG << "CORAL physical replica (1 of 1)" << std::endl;
      printOut( coralUrl, coralMode, middleTier,
                technology, server, schema, user, password, hidepw );

    }

  }

  // *** CATCH exceptions ***

  catch( std::exception& e )
  {
    LOG << "ERROR! Standard C++ exception: '" << e.what() << "'" << std::endl;
    return 1;
  }

  catch( ... )
  {
    LOG << "ERROR! Unknown exception caught" << std::endl;
    return 1;
  }

  // Successful program termination
  return 0;

}

//-----------------------------------------------------------------------------
