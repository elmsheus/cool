
// Local include files
#include "DummyTransactionMgr.h"

// Namespace
using namespace cool;

//-----------------------------------------------------------------------------

DummyTransactionMgr::DummyTransactionMgr()
  : m_isActive( false )
{
}

//-----------------------------------------------------------------------------

DummyTransactionMgr::~DummyTransactionMgr()
{
}

//-----------------------------------------------------------------------------
