Dear all,

following yesterday's discussion with Sven and Richard at the Monday 
meeting about the new 'user tag' functionality required for COOL_1_3_0,
I would like to propose to you the following slightly different 
(improved) specification and implementation design.

For reference: the original specification is the one I proposed 
in an e-mail message in April last year, which you can also find in
the COOL CVS doc repository in 
  http://cool.cvs.cern.ch/cgi-bin/cool.cgi/cool/RelationalCool/doc/userTagsSpec.txt

For the development team: the initial implementation design (later 
complemented by more email exchanges with Sven, not included) is also in 
  http://cool.cvs.cern.ch/cgi-bin/cool.cgi/cool/RelationalCool/doc/userTagsImpl.txt

Basically, last year's agreement was that users can specify AT MOST ONE
user tag when inserted a new IOV. From yesterday's discussion, I think
we still agree on this point.

However, last year's proposal also assumed that 'user tags' (those
specified when inserting a new IOV) are different from 'standard tags'
(those specified when tagging the HEAD of existing IOVs). From 
yesterday's discussion, this is the point which could still be improved.

I propose below a new functionality specification that goes in this 
direction. I split this in two parts: a summary of last year's proposal, 
plus a possible extension. The reason is that I am not sure we can 
implement the full picture for COOL_1_3_0.

Cheers

Andrea

===============================================
User tag example specification (AV 28.02.2006)
===============================================

General idea: an IOV can be inserted with AT MOST ONE associated user tag. 
This means that a consistent HEAD version must be kept (within the folder
and the channel) not only for ALL IOVS, but also for EACH subset of IOVS
associated to any given user tag.

Two specifications are discussed:
- In the 'old' (April 2005) specification, 'user tags' (those specified 
  when inserting a new IOV) are different from 'standard tags' (those 
  specified when tagging the HEAD of existing IOVs) and can NOT be mixed.
- In the 'new' (February 2006) specification, 'user tags' (those specified 
  when inserting a new IOV) are NOT different from 'standard tags (those 
  specified when tagging the HEAD of existing IOVs) and can be mixed.

[NB Each tag is independent of each other: it is NOT foreseen that one can 
ask for "the HEAD for tag A AND tag B, or the HEAD for tag A OR tag B".]

Example 1
----------
1. User inserts P1 payload in [  0, 100] with NO (user) tags
2. User inserts P2 payload in [ 10,  50] with (user) tag #1
3. User inserts P3 payload in [ 30,  80] with (user) tag #2
4. User inserts P4 payload in [ 20,  40] with (user) tag #1
5. User inserts P5 payload in [ 30,  70] with (user) tag #2

The following applies both to the old and new specifications.
The only difference is in terminology: in the old specification, 
these tags are 'user' tags (different type from 'standard' tags),
while in the new specification they are just 'tags'.

HEAD versions after last insertion:

All IOVS (irrespective of tags):
  P1 in [  0,  10]
  P2 in [ 10,  20]
  P4 in [ 20,  30]
  P5 in [ 30,  70]
  P3 in [ 70,  80]
  P1 in [ 80, 100]

(User) tag #1:
  P2 in [ 10,  20]
  P4 in [ 20,  40]
  P2 in [ 40,  50]

(User) tag #2:
  P5 in [ 30,  70]
  P3 in [ 70,  80]

Example 2 (old specification)
------------------------------
1. User inserts P1 payload in [  0,  50] with NO user tags
2. User inserts P2 payload in [ 50, 100] with NO user tags
3. User tags HEAD of folder with tag #2
4. User inserts P4 payload in [ 10,  50] with user tag #1
5. User inserts P5 payload in [ 30,  80] with user tag #2 - EXCEPTION
6. User inserts P6 payload in [ 20,  40] with user tag #1
7. User inserts P7 payload in [ 30,  70] with user tag #2 - EXCEPTION

In the old specification, step 5 and 7 here throw an exception: tag #2 is 
already reserved as a 'standard' tag and cannot be used as a 'user' tag.

HEAD versions after last insertion (ignoring step 5 and 7):

All IOVS (irrespective of tags):
  P1 in [  0,  10]
  P4 in [ 10,  20]
  P6 in [ 20,  30]
  P7 in [ 30,  70]
  P5 in [ 70,  80]
  P2 in [ 80, 100]

Tag #1:
  P4 in [ 10,  20]
  P6 in [ 20,  40]
  P4 in [ 40,  50]

User tag #2:
  P1 in [  0,  50]
  P2 in [ 50, 100]

Example 2 (new specification)
------------------------------
1. User inserts P1 payload in [  0,  50] with NO user tags
2. User inserts P2 payload in [ 50, 100] with NO user tags
3. User tags HEAD of folder with tag #2
4. User inserts P4 payload in [ 10,  50] with user tag #1
5. User inserts P5 payload in [ 30,  80] with user tag #2
6. User inserts P6 payload in [ 20,  40] with user tag #1
7. User inserts P7 payload in [ 30,  70] with user tag #2

In the new specification, no exceptions are thrown.

HEAD versions after last insertion:

All IOVS (irrespective of tags):
  P1 in [  0,  10]
  P4 in [ 10,  20]
  P6 in [ 20,  30]
  P7 in [ 30,  70]
  P5 in [ 70,  80]
  P2 in [ 80, 100]

Tag #1:
  P4 in [ 10,  20]
  P6 in [ 20,  40]
  P4 in [ 40,  50]

Tag #2:
  P1 in [  0,  30]
  P7 in [ 30,  70]
  P5 in [ 70,  80]
  P2 in [ 80, 100]

Example 3 (new specification)
------------------------------
 1. User inserts P1  payload in [  0,  50] with NO user tags
 2. User inserts P2  payload in [ 50, 100] with NO user tags
 3. User tags HEAD of folder with tag #2
 4. User inserts P4  payload in [ 10,  50] with user tag #1
 5. User inserts P5  payload in [ 30,  80] with user tag #2
 6. User inserts P6  payload in [ 20,  40] with user tag #1
 7. User inserts P7  payload in [ 30,  70] with user tag #2
 8. User tags HEAD of folder with tag #2
 9. User inserts P9  payload in [ 30,  80] with user tag #2
10. User inserts P10 payload in [ 20,  40] with user tag #1

In the new specification, no exceptions are thrown.

HEAD versions after last insertion:

All IOVS (irrespective of tags):
  P1  in [  0,  10]
  P4  in [ 10,  20]
  P10 in [ 20,  40]
  P9  in [ 40,  80]
  P2  in [ 80, 100]

Tag #1:
  P4  in [ 10,  20]
  P10 in [ 20,  40]
  P4  in [ 40,  50]

Tag #2:
  P1 in [  0,  10]
  P4 in [ 10,  20]
  P6 in [ 20,  30]
  P9 in [ 30,  80]
  P2 in [ 80, 100]

Essentially, tagging the HEAD (in step 8) with the same tag name 
previously used for inserting IOVs with attached tags (e.g. step 5)
logically erases all tag-to-IOV relations defined when inserting 
IOVs with attached tags (e.g. the tag attached in step 5 loses meaning).

-------------------------------------------------------------------------------

For reference: David Malon presented his use case in the presentation 
at the 2003 CondDB Workshop: http://agenda.cern.ch/fullAgenda.php?ida=a036470
