// Include files
#include <cstdio> // For sprintf on gcc45
#include "CoralBase/TimeStamp.h"
#include "CoolKernel/Exception.h"
#include "CoolKernel/Time.h"

// Namespace
using namespace cool;

//-----------------------------------------------------------------------------

Time::~Time()
{
}

//-----------------------------------------------------------------------------

Time::Time()
  : m_ts() // coral::TimeStamp() returns the current UTC time
{
}

//-----------------------------------------------------------------------------

Time::Time( int year,
            int month,
            int day,
            int hour,
            int minute,
            int second,
            long nanosecond ) :
  m_ts( year, month, day, hour, minute, second, nanosecond )
{
}

//-----------------------------------------------------------------------------

Time::Time( const Time& rhs ) :
  ITime( rhs ),
  m_ts( rhs.m_ts )
{
}

//-----------------------------------------------------------------------------

Time& Time::operator=( const Time& rhs )
{
  if ( this == &rhs ) return *this;  // Fix Coverity SELF_ASSIGN
  m_ts = rhs.m_ts;
  return (*this);
}

//-----------------------------------------------------------------------------

Time::Time( const ITime& rhs ) :
  m_ts( rhs.year(), rhs.month(), rhs.day(), rhs.hour(), rhs.minute(), rhs.second(), rhs.nanosecond() )
{
}

//-----------------------------------------------------------------------------

Time& Time::operator=( const ITime& rhs )
{
  m_ts = coral::TimeStamp( rhs.year(), rhs.month(), rhs.day(), rhs.hour(), rhs.minute(), rhs.second(), rhs.nanosecond() );
  return (*this);
}

//-----------------------------------------------------------------------------

Time::Time( const coral::TimeStamp& rhs )
  : m_ts( rhs )
{
}

//-----------------------------------------------------------------------------

Time& Time::operator=( const coral::TimeStamp& rhs )
{
  m_ts = rhs;
  return (*this);
}

//-----------------------------------------------------------------------------

std::ostream& Time::print( std::ostream& os ) const
{
  int year = this->year();
  int month = this->month(); // Months are in [1-12]
  int day = this->day();
  int hour = this->hour();
  int min = this->minute();
  int sec = this->second();
  long nsec = this->nanosecond();
  char timeChar[] = "yyyy-mm-dd_hh:mm:ss.nnnnnnnnn GMT";
  int nChar = std::string(timeChar).size();
  if ( snprintf( timeChar, 34, // Fix Coverity SECURE_CODING
                 "%4.4d-%2.2d-%2.2d_%2.2d:%2.2d:%2.2d.%9.9ld GMT",
                 year, month, day, hour, min, sec, nsec) == nChar ) {
    return os << std::string( timeChar );
  }
  std::stringstream s;
  s << "PANIC! Error printing out "
    << year << "-" << month << "-" << day << "_"
    << hour << ":" << min << ":" << sec << "." << nsec;
  throw Exception( s.str(), "Time" );
}

//-----------------------------------------------------------------------------

bool Time::operator==( const ITime& rhs ) const
{
  return ( m_ts == cool::Time( rhs ).coralTimeStamp() );
}

//-----------------------------------------------------------------------------

bool Time::operator>( const ITime& rhs ) const
{
  return ( m_ts > cool::Time( rhs ).coralTimeStamp() );
}

//-----------------------------------------------------------------------------
