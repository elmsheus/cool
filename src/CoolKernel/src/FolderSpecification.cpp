// Include files
#include <sstream>
#include <iostream> // Debug ROOT-7864
#include "CoolKernel/Exception.h"
#include "CoolKernel/FolderSpecification.h"

// Local include files

// Namespace
using namespace cool;

//-----------------------------------------------------------------------------

FolderSpecification::~FolderSpecification()
{
}

//-----------------------------------------------------------------------------

FolderSpecification::FolderSpecification( const IRecordSpecification& payloadSpecification )
  : m_versioningMode( FolderVersioning::Mode::SINGLE_VERSION )
  , m_payloadSpec( payloadSpecification )
  , m_payloadMode( PayloadMode::Mode::INLINEPAYLOAD )
{
}

//-----------------------------------------------------------------------------

FolderSpecification::FolderSpecification( FolderVersioning::Mode mode,
                                          const IRecordSpecification& pSpec,
                                          PayloadMode::Mode payloadMode )
  : m_versioningMode( mode )
  , m_payloadSpec( pSpec )
  , m_payloadMode( payloadMode )
{
  // Throw if an invalid versioning mode is specified (fix bug #103343)
  if ( mode != FolderVersioning::Mode::SINGLE_VERSION &&
       mode != FolderVersioning::Mode::MULTI_VERSION )
  {
    std::stringstream s;
    s << "Invalid versioning mode specified: " << mode;
    throw InvalidFolderSpecification( s.str(), "FolderSpecification" );
  }
  //std::cout << std::endl << "__DEBUG payloadMode=" << payloadMode
  //          << std::endl; // Debug ROOT-7864
  // Throw if an invalid payload mode is specified (fix bug #103351)
  if ( payloadMode != PayloadMode::Mode::INLINEPAYLOAD &&
       payloadMode != PayloadMode::Mode::SEPARATEPAYLOAD &&
       payloadMode != PayloadMode::Mode::VECTORPAYLOAD )
  {
    std::stringstream s;
    s << "Invalid payload mode specified: " << payloadMode;
    throw InvalidFolderSpecification( s.str(), "FolderSpecification" );
  }
  //std::cout << "__DEBUG " // Debug ROOT-7864
  //          << (payloadMode != PayloadMode::Mode::INLINEPAYLOAD) << " "
  //          << (payloadMode != PayloadMode::Mode::SEPARATEPAYLOAD) << " "
  //          << (payloadMode != PayloadMode::Mode::VECTORPAYLOAD)
  //          << std::endl;
}

//-----------------------------------------------------------------------------

/*
FolderSpecification::FolderSpecification( FolderVersioning::Mode mode,
                                          const IRecordSpecification& pSpec,
                                          const IRecordSpecification& cSpec )
  : m_versioningMode( mode )
  , m_payloadSpec( pSpec )
  , m_hasPayloadTable( false )
{
  // Throw if an invalid versioning mode is specified (fix bug #103343)
  if ( mode != FolderVersioning::Mode::SINGLE_VERSION &&
       mode != FolderVersioning::Mode::MULTI_VERSION )
  {
    std::stringstream s;
    s << "Invalid versioning mode specified: " << mode;
    throw InvalidFolderSpecification( s.str(), "FolderSpecification" );
  }
}
*///

//-----------------------------------------------------------------------------

FolderSpecification::FolderSpecification( const FolderSpecification& rhs )
  : m_versioningMode( rhs.m_versioningMode )
  , m_payloadSpec( rhs.m_payloadSpec )
  , m_payloadMode( rhs.m_payloadMode )
{
}

//-----------------------------------------------------------------------------

FolderSpecification& FolderSpecification::operator=( const FolderSpecification& rhs )
{
  m_versioningMode = rhs.m_versioningMode;
  m_payloadSpec = rhs.m_payloadSpec;
  m_payloadMode = rhs.m_payloadMode;
  return *this;
}

//-----------------------------------------------------------------------------

const FolderVersioning::Mode& FolderSpecification::versioningMode() const
{
  return m_versioningMode;
}

//-----------------------------------------------------------------------------

const IRecordSpecification& FolderSpecification::payloadSpecification() const
{
  return m_payloadSpec;
}

//-----------------------------------------------------------------------------

/*
const IRecordSpecification& FolderSpecification::channelSpecification() const
{
  return m_channelSpec;
}
*///

//-----------------------------------------------------------------------------

/*
RecordSpecification& FolderSpecification::channelSpecification()
{
  return m_channelSpec;
}
*///

//-----------------------------------------------------------------------------

const PayloadMode::Mode& FolderSpecification::payloadMode() const
{
  return m_payloadMode;
}

//-----------------------------------------------------------------------------
